package housecode.test.Notifications;

import android.app.IntentService;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;

import housecode.test.ChatElements.ChatMessage;
import housecode.test.ChatRoom;
import housecode.test.R;

public class PushChecker extends IntentService {

    private final int CONTENT_SIZE = 30;
    private final String CHANNEL_ID = "2213";

    private String _user;

    private ArrayList<Query> _listeners;
    private FirebaseDatabase _database;
    private ArrayList<DatabaseReference> _allChatRef;
    private int _givenId = 0;
    private NotificationManager _noteManager;

    @Override
    protected void onHandleIntent(Intent workIntent) {
        _database = FirebaseDatabase.getInstance();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            _user = workIntent.getStringExtra("User");
            CharSequence name = "Room notifications";
            String description = "Notifies you when a message is shown in a room you listen to";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            _noteManager = getSystemService(NotificationManager.class);
            _noteManager.createNotificationChannel(channel);
            listenRoom(workIntent.getStringExtra("RoomListen"));

        }

    }
    /*
    Listens to changes in the inputted room
    TODO have a method to stop listening
     */
    public void listenRoom(String room){
        DatabaseReference newRef = _database.getReference().child(room);
        Query newListener = newRef.child("Messages").orderByKey().limitToLast(1);
        final String roomForListener = room;
        newListener.addValueEventListener(new ValueEventListener() {
            private boolean _wentThroughFirst = false;
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                System.out.println("data changed");
                if(_wentThroughFirst) {//will otherwise display a notification when you send a message
                    try{
                        ArrayList<HashMap> newMessageList = (ArrayList<HashMap>)dataSnapshot.getValue();
                        if(newMessageList != null && newMessageList.get(0) != null){
                            ChatMessage newMessage = new ChatMessage(newMessageList.get(0));
                            if(!newMessage.nameSender.equals(FirebaseAuth.getInstance().getCurrentUser().getDisplayName())){
                                _createNotification(newMessage.content.substring(0, CONTENT_SIZE), roomForListener, _givenId++);
                            }
                        }
                    }
                    catch (ClassCastException e){
                        System.out.println("An error happened");
                    }
                }
                else {
                    System.out.println("not supposed to send a notification right now");
                    _wentThroughFirst = true;
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                //Don't think anything is needed to be done here
            }
        });
        if(_listeners == null){
            _listeners = new ArrayList<>();
            _allChatRef = new ArrayList<>();
        }
        _listeners.add(newListener);
        _allChatRef.add(newRef);
    }

    public PushChecker(){
        super("Display Notification");

    }


    private void _createNotification(String content, String title, int id){

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, Integer.toString(id));
        builder.setContentText(content);
        builder.setContentTitle(title);
        Intent openRoom = new Intent(this, ChatRoom.class);
        openRoom.putExtra("RoomName", title);
        PendingIntent sentIntent = PendingIntent.getActivity(this, 0, openRoom, 0);
        builder.setContentIntent(sentIntent);
    }
}
