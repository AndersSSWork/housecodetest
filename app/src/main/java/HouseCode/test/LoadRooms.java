package housecode.test;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import housecode.test.ChatElements.Room;
import housecode.test.Lists.RoomListAdapter;

public class LoadRooms extends AppCompatActivity {

    private ProgressBar _progress;

    private FirebaseDatabase _database;
    private DatabaseReference _roomRef;
    private FirebaseAuth _auth;

    private RecyclerView _displayedRooms;
    private RoomListAdapter _roomListAdapter;

    private Room[] _loadedRooms;

    public Context loadContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_load_rooms);
        loadContext = this;
        _progress = (ProgressBar)findViewById(R.id.loadR_progress);

        _displayedRooms = (RecyclerView)findViewById(R.id.loadR_rooms);
        _displayedRooms.setLayoutManager(new LinearLayoutManager(this));
        _roomListAdapter = new RoomListAdapter(_loadedRooms);
        _roomListAdapter.owner = LoadRooms.this;
        _displayedRooms.setAdapter(_roomListAdapter);

        _loadedRooms = new Room[0];

        _auth = FirebaseAuth.getInstance();
        //Redirect to the login screen
        if(_auth.getCurrentUser() == null){
           startActivity(new Intent(LoadRooms.this, LoginScreen.class));
           finish();
        }
        _database = FirebaseDatabase.getInstance();
        _roomRef = _database.getReference();

        Query roomQuery = _roomRef.child("AllRooms");
        roomQuery.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                try{
                    JSONArray roomJSON = new JSONArray((String)dataSnapshot.getValue());
                    _loadedRooms = new Room[roomJSON.length()];
                    for(int i = 0; i < roomJSON.length(); ++i){
                        _loadedRooms[i] = new Room((JSONObject)roomJSON.get(i));
                    }

                    _roomListAdapter.dataset = _loadedRooms;
                    _roomListAdapter.notifyDataSetChanged();
                    _progress.setVisibility(View.GONE);
                }
                catch (JSONException e){
                    //Create two temporary rooms and put them into the database
                    JSONArray newRoomJSON = new JSONArray();
                    Room roomOne = new Room("RoomOne", "First test room");
                    Room roomTwo = new Room("RoomTwo", "Second test room");
                    newRoomJSON.put(roomOne.toJSON());
                    newRoomJSON.put(roomTwo.toJSON());
                    _roomRef.child("AllRooms").setValue(newRoomJSON.toString());
                    _roomRef.child("RoomOne").setValue("not assigned properly");
                    _roomRef.child("RoomTwo").setValue("not assigned properly");

                    _loadedRooms = new Room[2];
                    _loadedRooms[0] = roomOne;
                    _loadedRooms[1] = roomTwo;

                    _roomListAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                ErrorDisplayer error = new ErrorDisplayer();
                error.displayErrorToast("Connection to database cancelled", loadContext);
            }
        });

    }
}
