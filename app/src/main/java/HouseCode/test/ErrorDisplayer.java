package housecode.test;

import android.content.Context;
import android.widget.Toast;

public class ErrorDisplayer {

    public void displayErrorToast(String text, Context context){
        Toast errorToast = Toast.makeText(context, text, Toast.LENGTH_LONG);
        errorToast.show();
    }
}
