package housecode.test;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import housecode.test.ChatElements.ChatMessage;
import housecode.test.Lists.ChatListAdapter;
import housecode.test.Lists.ChatListOnTop;
import housecode.test.Notifications.PushChecker;

public class ChatRoom extends AppCompatActivity {

    private ProgressBar _progressBar;

    private RecyclerView _displayedMessages;
    private ChatListAdapter _chatAdapter;
    private LinearLayoutManager _layoutManager;

    private FirebaseDatabase _database;
    private DatabaseReference _chatRef;
    private FirebaseAuth _auth;
    private Query _messageQuery;
    private ValueEventListener _currentListener;

    private TextView _TEMPText;

    private EditText _newMessageText;
    private String _typedMessage; //So that i can clear the _newMessageText before getting the new text

    private ArrayList<ChatMessage> _loadedMessages;

    private int _currentAmountMessages;
    private long _maxAmountMessages; //How many messages you are allowed to load
    public int amountScrolled = 0;
    //TODO make these changeable in a settings menu
    public final int FIRST_AMOUNT_MESSAGES = 50;
    public final int SINGLE_LOAD_AMOUNT_MESSAGES = 10;
    public final int NOT_ASSIGNED_PREF = 0,  ASSIGNED_FALSE = 1, ASSIGNED_TRUE = 2;

    public String openRoom;
    public Context chatContext;

    public final int READ_EXTERNAL_PERM_CODE = 2233;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_room);
        chatContext = this;
        _TEMPText = (TextView)findViewById(R.id.chat_TEMPText);
        _newMessageText = (EditText)findViewById(R.id.chat_post_text);
        _progressBar = (ProgressBar)findViewById(R.id.chat_progress);

        _displayedMessages = (RecyclerView)findViewById(R.id.chat_list);
        _loadedMessages = new ArrayList<>();
        _chatAdapter = new ChatListAdapter(_loadedMessages);
        _displayedMessages.setAdapter(_chatAdapter);
        _layoutManager = new LinearLayoutManager(this);
        _displayedMessages.setLayoutManager(_layoutManager);

        _chatAdapter.notifyDataSetChanged();

        _auth = FirebaseAuth.getInstance();
        if(_auth.getCurrentUser() == null){
            startActivity(new Intent(ChatRoom.this, LoginScreen.class));
            finish();
        }
        Intent intent = getIntent();
        openRoom = intent.getStringExtra("RoomName");
        _database = FirebaseDatabase.getInstance();
        _chatRef = _database.getReference().child(openRoom);
        _currentAmountMessages = FIRST_AMOUNT_MESSAGES;

        _listenAmountMes();
        _listenMessage();

        _messageQuery.addValueEventListener(_currentListener);
        ChatListOnTop topListener = new ChatListOnTop();
        topListener.callBack = this;
        _chatAdapter.onTopListener = topListener;

        _checkPreference();
    }

    public void loadMoreMessages(){
        if(_maxAmountMessages > _currentAmountMessages) {
            _messageQuery.removeEventListener(_currentListener);
            _currentAmountMessages += SINGLE_LOAD_AMOUNT_MESSAGES;
            if (_currentAmountMessages > _maxAmountMessages) {
                _currentAmountMessages = (int)_maxAmountMessages;
            }
            _messageQuery = _chatRef.child("Messages").orderByKey().limitToLast(_currentAmountMessages);
            _messageQuery.addValueEventListener(_currentListener);
        }
    }

    public void addMessages(ArrayList<HashMap> newMessages){
        if(newMessages != null) {
            _loadedMessages.clear(); //Otherwise would have a lot of duplicates around
            int addedSize = 0;
            for (HashMap singleMap : newMessages) {
                if (singleMap != null) {
                    _loadedMessages.add(new ChatMessage(singleMap));
                    ++addedSize;
                }
            }
            _displayedMessages.scrollToPosition(_loadedMessages.size() - 1);
            //notifyDataSetChanged must be run in UI thread which is what this ensures
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    _chatAdapter.notifyDataSetChanged();
                }
            });

        }
    }

    public void sendMessage(View placeholder){
        if(_newMessageText.getText().length() > 0) {
            _typedMessage = _newMessageText.getText().toString();
            Query setMessage = _chatRef.child("AmountMessages");

            ++_maxAmountMessages;

            Date currentDate = new Date();
            String strDateFormat = "hh:mm:ss a";
            DateFormat dateFormat = new SimpleDateFormat(strDateFormat);
            String formattedDate = dateFormat.format(currentDate);

            ChatMessage newMessage = new ChatMessage(_auth.getCurrentUser().getDisplayName(), formattedDate, _typedMessage, _maxAmountMessages);

            _chatRef.child("AmountMessages").setValue(_maxAmountMessages);
            _chatRef.child("Messages").child(Long.toString(newMessage.id)).setValue(newMessage);
            _newMessageText.setText("");
        }
    }

    public void openImagePicker(View view){
        /*
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_EXTERNAL_STORAGE
        )
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.READ_CONTACTS)) {
                //shows a permission this way
            } else {
                //get the permission
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, READ_EXTERNAL_PERM_CODE );
            }
        } else {
            // Permission has already been granted
            LinearLayout rowLayout = new LinearLayout(this);

            LinearLayout ll = new LinearLayout(this);
            ll.setOrientation(LinearLayout.VERTICAL);
            ll.setId(READ_EXTERNAL_PERM_CODE);

            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            SelectImage usedImage = SelectImage.setContext(this);
            fragmentTransaction.add(ll.getId(), usedImage, "Image Select");
            fragmentTransaction.commit();
            rowLayout.addView(ll);
        }
        */
        Toast.makeText(this, "Not Implemented, check TODOs", Toast.LENGTH_SHORT);
    }

    private void _listenAmountMes(){
        _messageQuery = _chatRef.child("Messages").orderByKey().limitToLast(_currentAmountMessages);
        Query getStartingAmountMessages = _chatRef.child("AmountMessages");
        getStartingAmountMessages.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                try {
                    _maxAmountMessages = (long) dataSnapshot.getValue();
                } catch (NullPointerException e) {
                    _maxAmountMessages = 0;
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                ErrorDisplayer error = new ErrorDisplayer();
                error.displayErrorToast("Connection to the amount of messages lost", chatContext);
            }
        });
    }

    private void _listenMessage(){
        _currentListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                try {
                    if(dataSnapshot.getValue() != null) {
                        if (dataSnapshot.getValue().getClass() == java.util.ArrayList.class) {
                            ArrayList<HashMap> newMessages = (ArrayList<HashMap>) dataSnapshot.getValue();
                            addMessages(newMessages);
                        } else {
                            ArrayList<HashMap> newMessages = new ArrayList<>();
                            newMessages.add((HashMap) dataSnapshot.getValue());
                            addMessages(newMessages);
                        }
                    }
                    if (_progressBar != null) {
                        _progressBar.setVisibility(View.GONE);
                        _progressBar = null;
                    }
                }
                catch (ClassCastException e){
                    ErrorDisplayer error = new ErrorDisplayer();
                    error.displayErrorToast("Invalid data received, try again and if problem persist contact system administrators", chatContext);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                ErrorDisplayer error = new ErrorDisplayer();
                error.displayErrorToast("Connection to messages lost", chatContext);
            }
        };
    }

    private void _checkPreference(){
        SharedPreferences preferences = this.getPreferences(Context.MODE_PRIVATE);
        int loadedPref = preferences.getInt(openRoom, NOT_ASSIGNED_PREF);
        if(loadedPref == NOT_ASSIGNED_PREF) {
            new AlertDialog.Builder(this)
                    .setTitle("Listen to this room?")
                    .setMessage("Get notifications from new messages")
                    .setPositiveButton("yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            _listenRoom();
                        }
                    })
                    .setNegativeButton("no", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            //TODO should save that you won't listen to this room
                        }
                    })
                    .show();
        }
        else if(loadedPref == ASSIGNED_TRUE){

            //ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
            //boolean isRunning = false;
            //for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            //if (PushChecker.class.getName().equals(service.service.getClassName())) {
            //isRunning = true;
            //break;
            //}
            //}
            //if(isRunning == false) {
            _listenRoom();
            // }
        }
    }

    private void _listenRoom(){
        SharedPreferences preferences = this.getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor prefEditor = preferences.edit();
        if(preferences.getInt(openRoom, 0) == 0){
            prefEditor.putInt(openRoom, 2);
        }
        PushChecker checker = new PushChecker();
        Intent checkIntent = new Intent(this, PushChecker.class);
        checkIntent.putExtra("RoomListen", openRoom);
        checkIntent.putExtra("User", _auth.getCurrentUser().getDisplayName());
        startService(checkIntent);
    }
}
