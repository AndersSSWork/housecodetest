package housecode.test.ChatElements;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class RoomContainer {

    public Room[] allRooms;


    public JSONArray toJSON()throws NullPointerException{
        if(allRooms == null){
            throw new NullPointerException("No rooms are assigned");
        }
        JSONArray toReturn = new JSONArray();
        for (Room singleRoom : allRooms) {
            toReturn.put(singleRoom.toJSON());
        }

        return toReturn;
    }

    @Override
    public String toString(){
        return toJSON().toString();
    }

    public RoomContainer(JSONArray inputtedRooms){
        if(inputtedRooms != null){
            allRooms = new Room[inputtedRooms.length()];
            for(int i = 0; i < inputtedRooms.length(); ++i){
                try {
                    allRooms[i] = new Room((JSONObject)inputtedRooms.get(i));
                }
                catch (JSONException e){
                    //TODO don't have null rooms in allRooms
                    allRooms[i] = null;
                }
            }
        }
    }
}
