package housecode.test.ChatElements;

import org.json.JSONException;
import org.json.JSONObject;

public class Room {

    public String nameRoom;
    public String description;

    @Override
    public String toString(){
        JSONObject toReturn = toJSON();
        if(toReturn != null) {
            return toReturn.toString();
        }
        else {
            return "";
        }
    }

    public JSONObject toJSON(){
        try {
            JSONObject roomJSON = new JSONObject();
            roomJSON.put("nameRoom", nameRoom);
            roomJSON.put("description", description);
            return roomJSON;
        }
        catch (JSONException e){
            return null;
        }
    }

    public Room(JSONObject newRoom){
        try{
            nameRoom = newRoom.getString("nameRoom");
            description = newRoom.getString("description");
        }
        catch (JSONException e){
            nameRoom = "JSONException";
            description = e.getMessage();
        }
    }

    public Room(String newName, String newDesc){
        nameRoom = newName;
        description = newDesc;
    }
}
