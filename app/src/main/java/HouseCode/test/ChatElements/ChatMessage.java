package housecode.test.ChatElements;

import android.media.Image;

import java.util.Date;
import java.util.HashMap;

public class ChatMessage {
    public String nameSender;
    public String dateSend;
    public String content;
    public long id;
    public Image avatarSender;
    public Image sendPic;

    public ChatMessage(String newSender, String newDate, String newContent, long newId, Image newAvatar, Image newPic){
        nameSender = newSender;
        dateSend = newDate;
        content = newContent;
        id = newId;
        avatarSender = newAvatar;
        sendPic = newPic;
    }

    public ChatMessage(String newSender, String newDate, String newContent, long newId){
        this(newSender, newDate, newContent, newId, null, null);
    }

    public ChatMessage(HashMap source){
        nameSender = (String)source.get("nameSender");
        content = (String)source.get("content");
    }

}
