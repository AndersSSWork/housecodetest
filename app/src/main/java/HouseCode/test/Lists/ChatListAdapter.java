package housecode.test.Lists;

import android.content.Intent;
import android.media.Image;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Date;

import housecode.test.ChatElements.ChatMessage;
import housecode.test.ChatElements.Room;
import housecode.test.ChatRoom;
import housecode.test.LoadRooms;
import housecode.test.R;

public class ChatListAdapter extends RecyclerView.Adapter<ChatListAdapter.ChatViewHolder> {

    public ArrayList<ChatMessage> dataset;
    public static LoadRooms owner;
    public IChatListOnTop onTopListener;

    public static class ChatViewHolder extends RecyclerView.ViewHolder {
        public TextView sender;
        public Date timeSend;
        public Image potImage; //Not guaranteed to be assigned
        public TextView content;


        public  ChatViewHolder(View newView){
            super(newView);
            sender = (TextView)newView.findViewById(R.id.chat_recycled_user);
            content = (TextView)newView.findViewById(R.id.chat_recycled_content);

        }
    }

    public ChatListAdapter(ArrayList<ChatMessage> newData){
        dataset = newData;
    }

    @Override
    public ChatListAdapter.ChatViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View singleView = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycled_chat, parent, false);
        return new ChatListAdapter.ChatViewHolder(singleView);

    }

    @Override
    public void onBindViewHolder(ChatListAdapter.ChatViewHolder holder, int position){
        if(position < getItemCount() && dataset.get(position) != null){
            holder.sender.setText(dataset.get(position).nameSender);
            holder.content.setText(dataset.get(position).content);
            if(position == 0){
                onTopListener.reachedTop();
            }
        }
    }

    @Override
    public int getItemCount(){
        if(dataset == null){
            return 0;
        }
        return dataset.size();
    }
}
