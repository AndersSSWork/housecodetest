package housecode.test.Lists;

import housecode.test.ChatRoom;

public class ChatListOnTop implements IChatListOnTop {
    public ChatRoom callBack;

    @Override
    public void reachedTop() {
        callBack.loadMoreMessages();
    }
}
