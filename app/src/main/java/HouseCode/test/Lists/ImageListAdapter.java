package housecode.test.Lists;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import housecode.test.ChatElements.Room;
import housecode.test.ChatRoom;
import housecode.test.LoadRooms;
import housecode.test.R;

public class ImageListAdapter extends RecyclerView.Adapter<ImageListAdapter.ImageViewHolder> {
    public ArrayList<String> dataset;
    public static Context owner;

    public static class ImageViewHolder extends RecyclerView.ViewHolder {
        public View view;
        public ImageView displayedImage;


        public ImageViewHolder(View newView){
            super(newView);
            view = newView;
            displayedImage = (ImageView)view.findViewById(R.id.imageC_image);

        }
    }

    public ImageListAdapter(ArrayList<String> newData){
        dataset = newData;
    }

    @Override
    public ImageListAdapter.ImageViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View singleView = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycled_image_choose, parent, false);
        return new ImageListAdapter.ImageViewHolder(singleView);

    }

    @Override
    public void onBindViewHolder(ImageListAdapter.ImageViewHolder holder, int position){
        if(position < getItemCount()){
            //Bitmap newImage = BitmapFactory.decodeFile(dataset[position]);
            //holder.displayedImage.setImageBitmap(newImage);
            //TODO should hae a default picture
            Glide.with(owner).load(dataset.get(position))
                    .into(holder.displayedImage);
        }
    }

    @Override
    public int getItemCount(){
        if(dataset == null){
            return 0;
        }
        return dataset.size();
    }
}
