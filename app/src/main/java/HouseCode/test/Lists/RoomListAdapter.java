package housecode.test.Lists;

import android.content.Intent;
import android.os.Debug;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import housecode.test.ChatElements.Room;
import housecode.test.ChatRoom;
import housecode.test.LoadRooms;
import housecode.test.R;

import static android.support.v4.content.ContextCompat.startActivity;

public class RoomListAdapter extends RecyclerView.Adapter<RoomListAdapter.RoomViewHolder> {

    public Room[] dataset;
    public static LoadRooms owner;

    public static class RoomViewHolder extends RecyclerView.ViewHolder {
        public View view;
        public TextView nameDisplay;
        public String nameRoom;
        public Button btn; //Seperate button because i don't trust people not to accidentally press while dragging


        public RoomViewHolder(View newView){
            super(newView);
            view = newView;
            nameDisplay = (TextView)view.findViewById(R.id.loadL_name);
            btn = (Button)view.findViewById(R.id.loadL_btn);
            btn.setOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent chatIntent = new Intent(owner, ChatRoom.class);
                            chatIntent.putExtra("RoomName", nameRoom);
                            owner.startActivity(chatIntent);
                        }
                    }
            );
        }
    }

    public RoomListAdapter(Room[] newData){
        dataset = newData;
    }

    @Override
    public RoomListAdapter.RoomViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View singleView = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycled_room, parent, false);
        return new RoomViewHolder(singleView);

    }

    @Override
    public void onBindViewHolder(RoomViewHolder holder, int position){
        if(position < getItemCount()){
            holder.nameDisplay.setText( dataset[position].nameRoom);
            holder.nameRoom = dataset[position].nameRoom;
        }
    }

    @Override
    public int getItemCount(){
        if(dataset == null){
            return 0;
        }
        return dataset.length;
    }
}
