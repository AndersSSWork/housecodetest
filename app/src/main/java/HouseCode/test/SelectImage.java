package housecode.test;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import housecode.test.Lists.ImageListAdapter;

public class SelectImage extends Fragment {
    private ArrayList<String> images;
    public Activity context;
    public View usedView;

    public static SelectImage setContext(Activity context){
        SelectImage returnValue = new SelectImage();
        returnValue.context = context;
        return returnValue;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        System.out.println("calling onCreateView");
        //TODO error: no view found for id 0x8b9 (R.layout.fragment_select_image)
        usedView = inflater.inflate(R.layout.fragment_select_image, container, false);
        loadImages();
        if(usedView == null){
            System.out.println("used view was null");
        }
        else {
            System.out.println("was not null here");
        }
        Button btn = (Button)usedView.findViewById(R.id.imageL_cancel);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancel();
            }
        });
        //loadImages();
        return usedView;
    }

    public void loadImages(){

        images = getAllShownImagesPath(context);
        RecyclerView usedRecycler = (RecyclerView) usedView.findViewById(R.id.imageL_recycler);
        usedRecycler.setLayoutManager(new LinearLayoutManager(context));
        ImageListAdapter adapter = new ImageListAdapter(images);
        adapter.notifyDataSetChanged();
    }

    private ArrayList<String> getAllShownImagesPath(Activity activity) {
        System.out.println("getting all shown images");
        Uri uri;
        Cursor cursor;
        int column_index_data, column_index_folder_name;
        ArrayList<String> listOfAllImages = new ArrayList<String>();
        String absolutePathOfImage = null;
        uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;

        String[] projection = { MediaStore.MediaColumns.DATA,
                MediaStore.Images.Media.BUCKET_DISPLAY_NAME };

        cursor = activity.getContentResolver().query(uri, projection, null,
                null, null);

        column_index_data = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
        column_index_folder_name = cursor
                .getColumnIndexOrThrow(MediaStore.Images.Media.BUCKET_DISPLAY_NAME);
        while (cursor.moveToNext()) {
            absolutePathOfImage = cursor.getString(column_index_data);

            listOfAllImages.add(absolutePathOfImage);
        }
        return listOfAllImages;
    }

    public void cancel(){
        System.out.println("was cancelled");
    }



}
