package housecode.test;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthCredential;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.android.gms.auth.api.*;

public class LoginScreen extends AppCompatActivity {

    private final int RC_SIGN_IN =1;
    private FirebaseAuth _auth;

    private SignInButton _btnGoogleLogin;
    public Context loginContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loginContext = this;
        setContentView(R.layout.activity_login_screen);
        _auth = FirebaseAuth.getInstance();
        _btnGoogleLogin = (SignInButton) findViewById(R.id.login_google_sign_in);
        _btnGoogleLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginGoogle(v);
            }
        });
    }


    public void loginFacebook(){
        //TODO I'm not giving them my phone number so can't make this at the moment
    }

    public void loginGoogle(View placeholder){
        //TODO the long id string is from google-services.json -> client -> oauth_client -> client_id is supposed to be getString(default_web_client_id)
        GoogleSignInOptions googleSignIn = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken("880145982006-mpjj39tu16nqbu1n8pj0ka48jj51g47s.apps.googleusercontent.com")
                .requestEmail().build();

        GoogleSignInClient mGoogleSignInClient = GoogleSignIn.getClient(this, googleSignIn);
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);

            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);
                _authenticateGoogleAcc(account);
            } catch (ApiException e) {
                ErrorDisplayer error = new ErrorDisplayer();
                error.displayErrorToast("Could not get google account", loginContext);
            }
        }
    }

    private void _authenticateGoogleAcc(GoogleSignInAccount account){
        AuthCredential credential = GoogleAuthProvider.getCredential(account.getIdToken(), null);
        _auth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            startActivity(new Intent(LoginScreen.this, LoadRooms.class));
                            finish();
                        } else {
                            // If sign in fails, display a message to the user
                            ErrorDisplayer error = new ErrorDisplayer();
                            error.displayErrorToast("Could not authenticate google account", loginContext);
                        }
                    }
                });

    }



}
