1 - Splash Screen - Functional
	Check if user exists
	Have a small spinning thing to show not frozen
	Redirection
	Asynchronous get - Suspended
	
2 - Login Screen - Functional
	Login parts
		Google
		Facebook - suspended
	Sign up parts
		Google
		Facebook - suspended
	
3 - Load rooms - Finished
	make sure Firebase is set up properly
	Get the rooms
	Set up the factory - changed structure
	Make it create 2 rooms when there aren't any made yet

4 - Single Room - Functional
	Set the message factory up - changed structure?
	Set up the list
	Prepare to have a send message part
	how single messages should look in the database
		be the child of the room
		their key is when they where put in
			first message is 1 second is 2 ...

5 - Send Message - Functional
	Minus sending picture for now

6 - Push notifier - Untested
	Figure out how to connect it with the rest
	Preference Menu to save what you are listening to

7 - Sending Picture - Cancelled
	Where to get them from
	Storing them
	Uploading them
	
8 - Cleanup and improvements - AndersSS
	Progress bar in Load room and chat room - Done
	Clean up the OnCreate method in ChatRoom - Done but might benefit from reorganizing
		Is way too big
	Load rooms might as well be the splash screen, as the current splash screen doesn't load anything time consuming - Done
		That's what happens when you make the structure before properly understanding the framework
	Clean up the flowchart in planning - Done
		Remove the builders and add the lists
	Fill the list of unsolved TODOS at the bottom of this file

9 - Quality of Life and optimization - For future development
	Settings menu
		Update time
		messages loaded
	Text parts
		Don't use the hard coded parts
	Allow for listening to several rooms
		I've already got a small commented out code snippet in end of ChatRoom.onCreate for how you might do it
			Would have to extend PushNotifier to do it
	Replace NotificationChannel with whatever works on apis lower than 26
	Reconsider if load rooms should use JSON arrays
		Originally used because i was unsure how firebase stored data in the database
		However since i can't get the amount of children in firebase it might actually be better to use JSONArray
	Don't use hardcoded strings for everything
		Make the facebook login
		I'll need a work phone before i can do this, they're not getting my private number
	Make the splashscreen load asynchronously
		Moved to step 9 as the SplashActivity is unnecessary
	Error messages - Functional but needs optimization
		hardcoded strings
	in Load rooms show those you get notifications from first
	A text message saying there are no messages
	A logout button
	The avatar of the user in the messages
	
Current Unsolved TODOS in project
	LoginScreen - 62
		I'm not giving them my phone number so can't make this at the moment
	LoginScreen - 66
		the long id string is from google-services.json -> client -> oauth_client -> client_id is supposed to be getString(default_web_client_id)
	ChatRoom - 63
		make these changeable in a settings menu
	ChatRoom - 273
		should save that you won't listen to this room
	SelectImage - 44
		error: no view found for id 0x8b9 (R.layout.fragment_select_image)
	ImageListAdapter - 59
		should have a default picture
	RoomContainer - 37
		don't have null rooms in allRooms